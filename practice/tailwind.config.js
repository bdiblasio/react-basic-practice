/** @type {import('tailwindcss').Config} */



module.exports = {
  content: [
    "./src/**/*.html",
    "./src/**/*.js",
    "./app.js/"
  ],
  theme: {
    extend: {
      fontFamily: {
        rock: ['rock salt', 'cursive']
      }
    },
  },
  plugins: [],
};

import { useState, React } from "react";


export const ClickTest = () => {
    let dogImage = "https://hips.hearstapps.com/hmg-prod/images/popular-puppy-questions-1639736471.jpg?crop=0.670xw:1.00xh;0.282xw,0&resize=1200:*"
    let catImage = "https://images.pexels.com/photos/1472999/pexels-photo-1472999.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
    let [picture, setPicture] =useState("")
    const catClick = () => setPicture (catImage)
    const dogClick = (event) => {
        if (event.detail === 2){
            setPicture (dogImage)
        }
    }
    const resetClick = () => setPicture("")



    return (
        <div className="container-1 border-2 border-red-400 bg-red-50 m-4 p-4">
            <p className="font-rock text-2xl"> What Picture Would you Like to See </p>
            <div className="image-container w-96 h-auto">
                <img src={picture}/>
            </div>
            <button onClick={catClick} className="border-2 border-red-400 p-1 mt-3 bg-red-100 w-48"> single click for cat </button>
            <button onClick={dogClick} className="border-2 border-red-400 p-1 ml-3  mt-3 bg-red-100 w-48"> double click for dog </button>
            <button onClick={resetClick} className="border-2 border-red-400 p-1 ml-3 mt-3 bg-red-100 w-48"> pls just reset </button>
        </div>

    )
}

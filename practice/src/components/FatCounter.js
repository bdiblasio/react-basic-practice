import {useState, React} from "react";

const FatCounter = () => {
    let [fatCount, setFatCount] = useState(100)

    function fatIncrease () {
        setFatCount (fatCount + 100)
    }

    function fatDecrease () {
      setFatCount (fatCount - 100)
    }

    return (
        <>
        <div className="border-2 border-green-400 bg-green-50 m-4 p-4">
          <p className="pb-5"> this is the same thing, but without an arrow function! </p>
          <div className="font-rock text-2xl"> {fatCount}</div>
          <div className="buttons">
            <button onClick = {fatIncrease} className="border-2 border-green-400 p-2"> Go Up ▲ </button>
            <button onClick = {fatDecrease} className="border-2 border-green-400 p-2 m-3"> Go Down ▼ </button>
          </div>
          </div>
    </>
    )
  }

export default FatCounter;


// First, we need to import the useState function, and React from the react library.
// I'm choosing useState, because it's a sleek way to manage state in this component
// We need to define our functional component, I'll call it Counter
// Now, lets add our first piece of state, called count.
// Count needs a counterpart, named setCount, which is a function that handles any updates to 'count'
// We pass the useState function the value 0, to initialize the 'count' variable (this could be any number)
// Next, we define two more functions, increase and decrease
// These functions

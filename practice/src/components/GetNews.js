import React, {useEffect, useState} from 'react';
import axios from 'axios';

function GetNews() {
    const [data, setData] = useState({results:[]}) //initializing wiht a default value. this is used to set the inital state of the variable so that upon the inital render, when the API data has not been fetched yet, we won't get 'not interable' errors.
    const [loading, setLoading] = useState(true)
    const [searchCount, setSearchCount] = useState(0)
    const [searchTerm, setSearchTerm] = useState("")

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get('https://api.nytimes.com/svc/topstories/v2/arts.json?api-key=keAGhT6Jg8zAlgIXKLS6XDL6UAGeJCsF')
                setData(response.data)
                setLoading(false)
            } catch (error) {
                console.error('Error Fetching Data!', error)
                setLoading(false)
            }
        }
        const throttle = setTimeout(() => {
            fetchData();
        }, 1000)

        return () => {
            clearTimeout(throttle)
        }
    }, [])

    useEffect(()=> {
        let count = 0
        for (let result of data.results){
            if (result.title.toLowerCase().includes(searchTerm)) {
                count++
            }
        }
        setSearchCount(count)
        setSearchTerm(searchTerm)
    }, [searchTerm]) //moved into separate effect so that it only mounted twice

    if (loading) {
        return <h1> loading.... </h1>
    }

    return (
        <>
        <div className="border-2 border-blue-400 bg-green-50 m-4 p-4">
            <h2 className="font-rock text-2xl"> NYT Trending Articles... </h2>
            <ul>
            {data.results.map((item, index) => (
                index > 0 &&
                    <li key={item.title}> <a href={item.url} className="text-blue-400 underline"> {item.title} </a></li>
                ))}
            </ul>
        </div>
        <div className="border-2 border-pink-400 bg-green-50 m-4 p-4">
        <form>
            <label htmlFor="searchTerm">
                <h1 className="font-rock text-2xl">Search the News:</h1>
                <input className="bg-lime-100 p-2 border-2 mt-5 w-3/6 mb-5" type="text" id="searchTerm" value={searchTerm} placeholder="" onChange={(e)=>setSearchTerm(e.target.value)}/>
            </label>
        </form>
        <p> "{searchTerm}" Appeared in Headlines {searchCount} Times Today.</p>
        </div>
        <div>

        </div>
        </>
    )
}

export default GetNews;

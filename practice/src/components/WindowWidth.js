import { useState, useEffect, React } from "react";

export default function WindowWidth() {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth)
    const [windowHeight, setWindowHeight] = useState(window.innerHeight)

    const handleResize = () => {
        setWindowWidth(window.innerWidth)
        setWindowHeight(window.innerHeight)
    }

    useEffect (() => {
        window.addEventListener('resize', handleResize)
    }, [])


    return (
        <div className="border-2 border-blue-500 bg-blue-50 m-4 p-4">
            <h1 className="font-rock text-2xl"> The Sizing of this Site is:</h1>
            <span> The Window Width is: {windowWidth}px</span><br/>
            <span> The Window Height is: {windowHeight}px</span>
        </div>
    )
}

import { useEffect, useState } from "react";

export function Timer() {
    const calculateTimeLeft = () => {
        let year = new Date().getFullYear();
        const difference = +new Date(`${year}-08-28`) - +new Date();
        let timeLeft = {};

        if (difference > 0) {
          timeLeft = {
            days: Math.floor(difference / (1000 * 60 * 60 * 24)),
            hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
            minutes: Math.floor((difference / 1000 / 60) % 60),
            seconds: Math.floor((difference / 1000) % 60),
          };
        }

        return timeLeft;
      };

      const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());
      const [year] = useState(new Date().getFullYear());

      useEffect(() => {
        setTimeout(() => {
          setTimeLeft(calculateTimeLeft());
        }, 1000);
      });

      const timerComponents = [];

      Object.keys(timeLeft).forEach((interval) => {
        if (!timeLeft[interval]) {
          return;
        }

        timerComponents.push(
          <span>
            {timeLeft[interval]} {interval}{" "}
          </span>
        );
      });
      return (
        <div className="border-2 border-orange-400 bg-yellow-50 m-4 p-4">
          <h1 className="font-rock text-2xl">Technical Interview </h1>
          <h2 className="mb-5"> good luck! </h2>
          <span className="font-mono bg-black text-green-400 p-2 border-2 border-lime-600 outline-2 outline-offset-4 outline-dashed">
          {timerComponents.length ? timerComponents : <span>Time's up!</span>}
          </span>
        </div>
      );
    }





  // useEffect updates the amount of time remaining
  // React default will invoke the effect on every render
  // Every time timeLeft is updated, the useEffect fires
  // Every time it fires, we set a timer for 1 second which will update the timeLeft after that (1second) time elapses
  // And so on, and so on..
  // To prevent stacking timeouts, clearTimeout is added
  // Return funciton runs everytime useEffect runs Timer, except for on the first run

import { useEffect, useState, React } from "react";
import { TarotCards } from "../data/TarotCards";
import TarotCardCheckBox from "./TarotCheckBox";


const SearchBar = () => {
    const [searchTerm, setSearchTerm] = useState("");
    const [filteredData, setFilteredData] = useState(TarotCards);

    const handleSearchChange = (e) => {
      const searchValue = e.target.value;
      setSearchTerm(searchValue);

      const filtered = TarotCards.filter((item) =>
        Object.values(item).some((fieldValue) =>
          String(fieldValue).toLowerCase().includes(searchValue.toLowerCase())
        )
      );
      setFilteredData(filtered);
    };

    return (
      <div className="border-2 border-green-800 bg-green-50 m-4 p-4 shadow-lg shadow-lime-200 ">
        <h1 className="font-rock text-2xl">
            Please Search for a Tarot Card
        </h1>
        <input
          type="text"
          placeholder="by name, arcana, or suit"
          value={searchTerm}
          onChange={handleSearchChange}
          className="bg-lime-100 p-2 border-2 mt-5 w-3/6 mb-5"
        />
        {searchTerm ? (
          <table className="border-2 w-3/6">
            <thead className="mt-1 p-4">
              <tr className="p-3 mt-5 bg-blue-400">
                <th>Name</th>
                <th>Arcana</th>
                <th>Suit</th>
              </tr>
            </thead>
            <tbody className="">
              {filteredData.map((item, index) => (
                <tr key={index} className="border-2 border-blue-500">
                  <td className="pl-4 w-48">{item.name}</td>
                  <td className="w-48">{item.arcana}</td>
                  <td className="w-48">{item.suit}</td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          ""
        )}

      </div>
    );
  };

  export default SearchBar;

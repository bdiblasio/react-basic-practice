import { useState, React } from "react";
import { TarotCards } from "../data/TarotCards";

const TarotCardCheckBox = ({}) => {
  const [isMajorArcana, setIsMajorArcana] = useState(false);
  const [isMinorArcana, setIsMinorArcana] = useState(false);
  const [isCups, setIsCups] = useState(false);
  const [isSwords, setIsSwords] = useState(false);
  const [isWands, setIsWands] = useState(false);
  const [isPentacles, setIsPentacles] = useState(false);

  const filteredData = TarotCards.filter((card) => {
    if (
      (isMajorArcana && card.arcana === "Major") ||
      (isMinorArcana && card.arcana === "Minor") ||
      (isCups && card.suit === "Cups") ||
      (isSwords && card.suit === "Swords") ||
      (isWands && card.suit === "Wands") ||
      (isPentacles && card.suit === "Pentacles")
    ) {
      return true;
    }
    return false;
  });

  return (
      <div className="border-2 border-green-800 bg-green-50 m-4 p-4 shadow-lg shadow-lime-200 ">
        <h1 className="font-rock text-2xl mb-5"> Or Check a Box </h1>

      <label className="pr-5">
        <input
          type="checkbox"
          className="mb-10"
          checked={isMajorArcana}
          onChange={() => setIsMajorArcana(!isMajorArcana)}
        />
        Major Arcana
      </label>

      <label className="pr-5">
        <input
          type="checkbox"
          checked={isMinorArcana}
          onChange={() => setIsMinorArcana(!isMinorArcana)}
        />
        Minor Arcana
      </label>

      <label className="pr-5">
        <input
          type="checkbox"
          checked={isCups}
          onChange={() => setIsCups(!isCups)}
        />
        Cups
      </label>

      <label className="pr-5">
        <input
          type="checkbox"
          checked={isWands}
          onChange={() => setIsWands(!isWands)}
        />
        Wands
      </label>

      <label className="pr-5">
        <input
          type="checkbox"
          checked={isPentacles}
          onChange={() => setIsPentacles(!isPentacles)}
        />
        Pentacles
      </label>

      <label>
        <input
          type="checkbox"
          checked={isSwords}
          onChange={() => setIsSwords(!isSwords)}
        />
        Swords
      </label>
{ isMajorArcana || isMinorArcana || isCups || isWands || isPentacles || isSwords ? (
      <table className="border-2 w-3/6">
        <thead className="mt-1 p-4">
          <tr className="p-3 mt-5 bg-blue-400">
            <th>Name</th>
            <th>Arcana</th>
            <th>Suit</th>
          </tr>
        </thead>
        <tbody className="">
          {filteredData.map((card, index) => (
            <tr key={index} className="border-2 border-blue-500">
              <td className="pl-4 w-48">{card.name}</td>
              <td className="w-48">{card.arcana}</td>
              <td className="w-48">{card.suit}</td>
            </tr>
          ))}
        </tbody>
      </table>
) : ( "" )
          }
    </div>
  );
};

export default TarotCardCheckBox;

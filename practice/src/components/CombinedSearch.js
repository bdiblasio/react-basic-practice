import React, { useState } from "react";
import SearchBar from "./TarotSearch"; // Import your SearchBar component
import TarotCardCheckBox from "./TarotCheckBox"; // Import your TarotCardCheckBox component

const CombinedFilter = ({ TarotCards }) => {
  // Use state to manage filtered data
  const [filteredData, setFilteredData] = useState(TarotCards);

  // Define a function to update filtered data based on search and checkboxes
  const updateFilteredData = (newFilteredData) => {
    setFilteredData(newFilteredData);
  };

  return (
    <div>
      <SearchBar
        TarotCards={TarotCards}
        updateFilteredData={updateFilteredData}
      />
      <TarotCardCheckBox
        TarotCards={TarotCards}
        updateFilteredData={updateFilteredData}
      />
      {/* Render your filteredData here */}
      <table className="border-2 w-3/6">
            <thead className="mt-1 p-4">
              <tr className="p-3 mt-5 bg-blue-400">
                <th>Name</th>
                <th>Arcana</th>
                <th>Suit</th>
              </tr>
            </thead>
            <tbody className="">
              {filteredData.map((item, index) => (
                <tr key={index} className="border-2 border-blue-500">
                  <td className="pl-4 w-48">{item.name}</td>
                  <td className="w-48">{item.arcana}</td>
                  <td className="w-48">{item.suit}</td>
                </tr>
              ))}
            </tbody>
          </table>
    </div>
  );
};

export default CombinedFilter;

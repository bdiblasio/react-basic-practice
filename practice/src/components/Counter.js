import {useState, React} from "react";

const Counter = () => {
    let [count, setCount] = useState(0)
    const increase = () => setCount (count + 1)
    const decrease = () => setCount (count - 1)

    return (
        <>
        <div className="border-2 border-green-400 bg-green-50 m-4 p-4">
          <div className="font-rock text-2xl"> {count}</div>
          <div className="buttons">
            <button onClick = {increase} className="border-2 border-green-400 p-2"> Go Up ▲ </button>
            <button onClick = {decrease} className="border-2 border-green-400 p-2 m-3"> Go Down ▼ </button>
          </div>
          </div>
    </>
    )
  }

export default Counter;



// First, we need to import the useState function, and React from the react library.
// I'm choosing useState, because it's a sleek way to manage state in this component
// We need to define our functional component, I'll call it Counter
// Now, lets add our first piece of state, called count.
// Count needs a counterpart, named setCount, which is a function that handles any updates to 'count'
// We pass the useState function the value 0, to initialize the 'count' variable (this could be any number)
// Next, we define two more functions, increase and decrease
// These functions will manipulate the data in setCount to be whatever the sum of (count +/- 1) is
// Skipping down to the buttons, because the rest is pretty basic HTML with tailwind...
// Each button has an onClick attribute that will specify what function to call when its clicked.

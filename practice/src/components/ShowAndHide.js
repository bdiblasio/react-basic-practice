import { useState, React } from "react";

export const ShowAndHide = () => {
  let [show, setShow] = useState("");
  const ToggleShow = () => {
    setShow(!show);
  };

  return (
    <div className="show-container border-2 border-blue-400 bg-blue-50 m-4 p-4">
      <button onClick={ToggleShow} className="bg-blue-200 border-2 border-blue-500 p-4">
        {" "}
        {show ? "Nothing too Exciting Happened..." : "Curious? Click it :)"}{" "}
      </button>
      {show ? (
        <>
          <p> just a hidden DIV </p>
        </>
      ) : (
        <p></p>
      )}
    </div>
  );
};

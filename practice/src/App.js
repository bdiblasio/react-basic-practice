import { ClickTest } from "./components/ClickTest";
import { ShowAndHide } from "./components/ShowAndHide";
import Counter from "./components/Counter";
import { Timer } from "./components/Timer";
import APICall from "./components/APICall";
import WindowWidth from "./components/WindowWidth";
import SearchBar from "./components/TarotSearch";
import TarotCardCheckBox from "./components/TarotCheckBox";
import FatCounter from "./components/FatCounter";
import GetNews from "./components/GetNews";

function App() {
  return (
    <div className="App">
      <Counter />
      <FatCounter/>
      <ClickTest />
      <ShowAndHide />
      <Timer />
      <WindowWidth />
      <APICall />
      <SearchBar />
      <TarotCardCheckBox/>
      <GetNews/>
    </div>
  );
}

export default App;
